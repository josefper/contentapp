#!/usr/bin/python

import socket


class webApp:

    def parse(self, request):
        """"Parseo la peticion, extrayendo la info"""

        parsed_request = request.split()[1].split('/')
        if parsed_request[1] == "":
            print("parse: No hay nada que parsear ahora mismo")

        return parsed_request

    def process(self, parsed_request, recursos):
        """Procesa los datos de la peticion
        Devuelve el codigo HTTP de la respuesta y una pagina HTML
        """

        html_respuesta = "<html><body>Pagina sin recurso</body></html>"
        if parsed_request[1] == "":
            print("process: no hay nada que procesar")
        elif parsed_request[1] == "favicon.ico":
            html_respuesta = "<h1>No hay favicon</h1>"
            print("No hay favicon!!!!")
        elif parsed_request[1] in recursos:
            html_respuesta = recursos[parsed_request[1]]
            print("process: Encontrado recurso")
        else:
            print("process: No entiendo lo que me has pasado")

        return "200 OK", html_respuesta

    def __init__(self, hostname, port, recursos):
        """Iniciar nuestra aplicacion web"""

        self.hostname = hostname
        self.port = port
        self.recursos = recursos

        # Configuracion del socket
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((self.hostname, self.port))
        mySocket.listen(5)

        while True:
            print("Esperando alguna conexion...")
            connectionSocket, addr = mySocket.accept()
            print("Conexion recibida de: " + str(addr))
            recibido = connectionSocket.recv(2048)
            print("El crudo en bytes: ", recibido)

            # Manejar peticion, los recursos
            parsed_request = self.parse(recibido.decode('utf-8'))

            # Proceso la peticion y creo la respuesta
            return_code, html_respuesta = self.process(parsed_request, self.recursos)

            # Enviar respuesta
            # Codificar a UTF-8 para poder enviar el mensaje.
            respuesta = "HTTP/1.1 " + return_code + "\r\n\r\n" \
                        + html_respuesta + "\r\n"
            connectionSocket.send(respuesta.encode('utf-8'))
            connectionSocket.close()
