#!/usr/bin/python

import socket


PAGE = """
<html>
    <body>
        {content}
        <hr>
        {format}
    </body>
</html>
"""

PAGE_NOT_FOUND = """
<html>
    <body>
        Resource not found: {method}
        <hr>
        {format}
    </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<html>
    <body>
        Method not found: {method}
        <hr>
        {format}
    </body>
</html>
"""

class webApp:

    def parse(self, request):
        """"Quedarme con el tipo de accion, el nombre de recurso y el contenido"""
        data = {}
        body_start = request.find('\r\n\r\n')

        if body_start == -1:
            # No hay cuerpo en la peticion
            data['body'] = None
        else:
            # Hay cuerpo, me lo quedo
            data['body'] = request[body_start+4:]

        parts = request.split('', 2)[1]
        data['method'] = parts[0]
        data['resource'] = parts[1]

        return data

    def process(self, parsed_request):
        """Procesa con el recurso de la respuesta"""

        html_respuesta = "<html><body>Pagina sin recurso</body></html>"
        http_code = "200 OK"
        if parsed_request['method'] == 'GET':
            http_code, html_respuesta = self.get(parsed_request['resource'])
        elif parsed_request['method'] == 'PUT':
            http_code, html_respuesta = self.put(parsed_request['resource'], parsed_request['body'])
        else:
            # Por si me llega un metodo que no controlo
            http_code = "405 METHOD NOT ALLOWED"
            html_page = PAGE_NOT_ALLOWED.format(method=parsed_request['method'])

        return http_code, html_respuesta

    def put(self, resource, body):
        # Actualizar/Crear diccionario contenido
        self.contents[resource] = body
        html_page = PAGE.format(content=body)
        http_code = "200 OK"
        return http_code, html_page

    def get(self, resource):
        if resource in self.contents:
            content = self.contents[resource]
            html_page = PAGE.format(resorce=resource)
            http_code = "200 OK"
        else:  # Si no encuentra el recurso salta el recurso error
            content = self.contents["/error"]
            html_page = PAGE.format(resorce=resource)
            http_code = "404 RESOURCE NOT FOUND"

        return http_code, html_page

    def __init__(self, hostname, port):
        """Iniciar nuestra aplicacion web"""

        self.hostname = hostname
        self.port = port
        self.contents = {"hola": "<html><body>Hola mundo!</body></html>", "adios": "<html><body>Adios mundo "
                                                                                  "cruel!</body></html>"}

        # Configuracion del socket
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((self.hostname, self.port))
        mySocket.listen(5)

        while True:
            print("Esperando alguna conexion...")
            connectionSocket, addr = mySocket.accept()
            print("Conexion recibida de: " + str(addr))
            recibido = connectionSocket.recv(2048)
            print("El crudo en bytes: ", recibido)

            # Manejar peticion, los recursos
            parsed_request = self.parse(recibido.decode('utf-8'))

            # Proceso la peticion y creo la respuesta
            return_code, html_respuesta = self.process(parsed_request)

            # Enviar respuesta
            # Codificar a UTF-8 para poder enviar el mensaje.
            respuesta = "HTTP/1.1 " + return_code + "\r\n\r\n" \
                        + html_respuesta + "\r\n"
            connectionSocket.send(respuesta.encode('utf-8'))
            connectionSocket.close()
